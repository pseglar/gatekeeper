# Gatekeeper Setup

----

## Installing node at Raspberry Pi.

- Delete pre-installed node by default.

`sudo apt-get remove node`

- Detect the version of ARM hardware.

 `uname -m`
 
 - Install the matched version of node via **package manager** or via **tarball method**. 

#### 1- Installing node using package manager:

```sh
curl -sL https://deb.nodesource.com/setup_X.X | sudo -E bash 
sudo apt-get install -y nodejs
````

- Install build tools for compile and install native addons from npm.

`sudo apt-get install -y build-essential
`

#### 2- Installing node using tarball method: 

#### Example ARMv6: (version not published at package manager)

>*You appear to be running on ARMv6 hardware. Unfortunately this is not currently supported by the NodeSource Linux distributions. Please use the ‘linux-armv6l’ binary tarballs available directly from nodejs.org for Node.js v4 and later.*


```sh
wget htttps//nodejs.org/dist/<path-version>/node-vX.X.X-linux-armv6l.tar.xz
tar xvf node-vX.X.X-linux-armv6l.tar.xz
cd xvf node-vX.X.X-linux-armv6l
sudo cp -R bin/* /usr/bin/
sudo cp -R lib/* /usr/lib/
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install build-essential
```
----
### Copying *Gatekeeper* repository to Raspberry via SSH.

1. Get Raspberry Pi IP. ( inet addr )
`ifconfig` or `hostname -I` (if the network is working)
2. On MacOs Terminal
`ssh pi@XX.XX.XX.XX` (IP)

3. Insert Password.

4. In another tab go to our cloned repository and do a secure copy
`scp -r <our-directory> pi@xx.xx.xx.xx:/<pi-parent-directory-path>`

5. OR: Copy files with **rsync** over SSH:
    - Install rsync on Raspbian `sudo apt-get install rsync`
    - Synchronize local folder on remote server:
    ```sh
    rsync -r -a -v -e ssh --delete <local-path-or-file> pi@X.X.X.X:/<pi-parent-directory-path>
    ```

>**Notes:** 
>- If "/" is placed at the end of the source folder, rsync will copy one the content of the folder, else will copy the folder itself and the content of the folder.
>- If "/" is placed at the end of destination folder, rsync will paste the data directly inside the folder, else it will create a folder with that name and paste the data inside that folder.
> [Reference](https://kyup.com/tutorials/copy-files-rsync-ssh/)

----

### Installing the *Gatekeeper* repository on Raspberry Pi.

1. Install or upgrade npm
`sudo apt-get install npm@latest -g`
2. Install repo  
 `npm install`
3. Run repo
  `npm start`
  or
  `nodemon start`
  or
  `node ./bin/www`
---

## Node Production Process Manager.

1. Install [PM2](http://pm2.keymetrics.io/)
``` npm install pm2 -g```
2. Execute this command ```pm2 startup ``` to save your process list and bring it back at machine restarts (edit the output):
You simply have to copy/paste the line PM2 gives you and the startup script will be configured for your OS.


```sh
Output example: [PM2] You have to run this command as root. Execute the following command:
sudo su -c "env PATH=$PATH:/home/unitech/.nvm/versions/node/v4.3/bin pm2 startup <distribution> -u <user> --hp <home-path>
```
3. Start the project 
``` pm2 start my-project.js```
4. Save this process to startup process list
  ```pm2 save```

> Note from PM2 documentation: When updating nodejs, the pm2 binary path might change (it will necessarily change if you are using nvm). Therefore, we would advise you to run the startup command after any update


----
## Troubleshooting
----

### [Epoll](https://github.com/fivdi/epoll) module errors while installing node repository.

`Error: Module did not self-register.`

- **Mac OS X doesn't support Epoll. 
(It's a Linux kernel system call)**

    - Run the repository directly at Raspberry Pi.
    - Run the respository in a Virtual Machine (Raspbian iso emulated in ARM with Qemu [-see documentation-](/other_file.md)).
    - Use [kqueue](https://en.wikipedia.org/wiki/Kqueue) instead. 

- **Epoll supports only Node.js versions 4, 6, 8 and 10.** 
(last check: 4/01/2019)
     
     - downgrade Node to v.8.x.x

----

### [Node-hid](https://github.com/node-hid/node-hid) errors while installing node repository.

1. **On Unix**: Make sure that our **gcc** (GNU compiler) version it's compatible with node version.

  
  
    Node v.8.x.x minimum GCC version 4.9.4

```sh
sudo apt-get install gcc-x.x g++-x.x
export CXX=g++-x.x
npm install node-hid
```

[stackoverflow issue](https://github.com/node-hid/node-hid/issues/115#issuecomment-146388136)


2. **On MacOs**: Make sure **Xcode** is installed (contains gcc):
`xcode-select --install`

----

### Another Node-hid module error while installing node repository [Build error at Hidapi](https://github.com/paritytech/parity-ethereum/issues/4546)

```js
'/home/pi/gatekeeper/node_modules/node-hid/build'
  CC(target) Release/obj.target/hidapi/hidapi/linux/hid.o
../hidapi/linux/hid.c:44:21: fatal error: libudev.h: No existe el fichero o el directorio
 #include <libudev.h>
                    ^
```
1. Erase *node_modules*.

2. Install and reinstall:

```sh
sudo npm install -g node-gyp
sudo apt-get install build-essential git
```

3. Install **libudev-dev**

    
`sudo apt-get install libudev-dev`

4. Install the repo again

----

### [Libusb](https://github.com/libusb/libusb) module errors while installing node repository.

1. Erase *node_modules*.
2. Install **libusb**

`sudo apt-get install libusb-1.0-0-dev`

3. Install the repo again.

----


 
### *"Cannot open device with path xxxx:xxxx:xxxx"* error when node starts. 
or
### *"Cannot open device with vendor id XXXX and product id XXXX"* error when node start.

 ```js
 /home/pi/gatekeeper/node_modules/node-hid/nodehid.js:54
    this._raw = new (Function.prototype.bind.apply(binding.HID,
                ^
  ```

#### (Node-hid access to HID related)

>This is a problem related with the [udev](https://wiki.archlinux.org/index.php/udev#List_the_attributes_of_a_device) device permissions. Most Linux distros had the USB HID devices owned by the root. We must to create a udev rule, usually based on *vendorId* and *productId*.

### There are two ways to face it:

- #### Set the udev permissions accessible to any connected device and to all users. 
  ##### [setted for the two node-hid drivers, libsub and hidraw]

1. Place the rule file text at:

    
    /etc/udev/rules.d

2. Set this params:

```js
SUBSYSTEM=="hidraw", MODE="0007"
SUBSYSTEM=="usb", MODE="0007"
```

3. Save as:

    
    /etc/udev/rules.d/51-<chosen-name>.rules
 
 
 4. Reload rules
 
  
```sh 
sudo udevadm control --reload-rules
```
  
5. Unplug and plug the device or reboot system.

- #### Set the udev permissions for a specific device.


A) Example (**important: idVendor and idProduct: hex and lowercase!**): [Setted for **libsub** driver]


```js 
SUBSYSTEM=="usb", 
ATTR{idVendor}=="0c2e", 
ATTR{idProduct}=="0200", 
MODE="0777"
SUBSYSTEM=="usb", 
ATTR{configuration}=="HID Keyboard", 
MODE="0777"
```

B) Example (**important: idVendor and idProduct: hex and lowercase!**): [Setted for **hidraw** driver]


```js 
SUBSYSTEM=="hidraw", 
ATTR{idVendor}=="0c2e", 
ATTR{idProduct}=="0200", 
MODE="0777"
SUBSYSTEM=="hidraw", 
ATTR{configuration}=="HID Keyboard", 
MODE="0777"
```


### **How to know the device attributes?**

```js
lsusb // show all devices at USB bus
udevadm info -a /dev/bus/usb/001/00X // change X, put device number instead
```
Alternative:

```sh
usb-devices
```
 
### How  to know the driver?
  
  - Import at scanner index.js 
  ```js
  const getDevices = require("usb-barcode-scanner").getDevices;
  ```

  - Use the function **getDevices()** and console the output.

### How to change the driver?
  
  - Reinstall node-hid with libusb:

`npm install node-hid --build-from-source --driver=libusb`
  
  - Reinstall node-hid with hidraw:

`npm install node-hid --build-from-source --driver=hidraw`
  
----