# Gatekeeper (carril rápido) project Documentation
---

## Kiosk mode & auto boot. 
### [RASPBERRY PI SETUP](RASPBERRYPI_SETUP.md)

----
 ## Install and configure display.

### [TOUCH SCREEN DISPLAY SETUP](TOUCHSCREEN_SETUP.md)

---
## Install repository project, handle processes and troubleshooting.

### [GATEKEEPER PROJECT SETUP](GATEKEEPER_SETUP.md)
---