const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const i18n = require('i18n');
const bodyParser = require('body-parser');
const app = express();
const scannerApi = require('./providers/scanner');
const axios = require('axios');
const CONFIG = require('./config');
const { GATE_ID, BASE_URL } = CONFIG;

i18n.configure({
  locales: ['en', 'es', 'ca', 'fr'],
  directory: __dirname + '/locales',
  defaultLocale: 'es',
  objectNotation: true // necessary to be able to hierarchize in translations. Example: access main.welcome
});

axios.defaults.baseURL = BASE_URL;
axios.defaults.headers.common.Authorization = GATE_ID + 'Q1W2E3';
axios.defaults.timeout = 30000;

//Routes
const {
  mainView,
  manualView,
  openingError,
  reservationError,
  successView,
  setLanguage,
  handleOpenGateRouter,
  statusCheckRouter,
  warningView } = require('./routes');

scannerApi.scanner.startScanning();
scannerApi.startListening();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//app.use(logger('dev'));
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
//app.use(bodyParser.urlencoded({ extended: false })); 
//app.use(bodyParser.json()); 
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'), { maxAge: '30 days' })); // Atention!: remove maxAge when modify CSS, this is the cache
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(i18n.init);


// Render routes

app.use('/', mainView);
app.use('/manual-opening', manualView);
app.use('/success', successView);
app.use('/opening-error', openingError);
app.use('/reservation-error', reservationError);
app.use('/setLocale/:language(en|es|ca|fr)', setLanguage);
app.use('/warning', warningView);

//Providers routes

app.use('/open', handleOpenGateRouter);
app.use('/300', statusCheckRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;