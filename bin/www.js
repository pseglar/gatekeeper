const app = require('../app');
const debug = require('debug')('gatekeeper:server');
const socketApi = require('../providers/socket/socket');
const { PORT } = process.env;

// Environment.

const port = normalizePort(PORT || '3000');
app.set('port', port);

// Create and listen http server and sockets.

const server = require('http').createServer(app);
const io = socketApi.io.listen(server);

server.listen(port);

server.on('error', onError);
server.on('listening', onListening);

io.set('heartbeat timeout', 5000);
io.set('heartbeat interval', 2000);

// Normalize a port into a number, string, or false.

function normalizePort(val) {
  const port = parseInt(val, 10);
  if (isNaN(port)) return val;
  if (port >= 0) return port;
  return false;
}

// Event listener for HTTP server "error" event.

function onError(error) {
  if (error.syscall !== 'listen') throw error;

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // Handle specific listen errors with friendly messages.

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// Event listener for HTTP server "listening" event.

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Server listening on ' + bind);
}

process.listeners('uncaughtException', (err) => {
  console.log('process listener uncaughtException');
  throw err;
});