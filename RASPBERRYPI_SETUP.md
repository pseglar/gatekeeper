# Raspberry Pi Setup

## Run Web Browser in Kiosk Mode

Documentation based in this great [Reference](https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/)

1. Install Raspbian Lite.
2. Boot up Raspberry Pi, login and start ```sudo raspi-config```
    - **Localisation Options**: select prefered locale, timezone and keyboard layout.
    - **Change User password.**
    - **Network Options** configure Wifi.
    - **Boot Options** > Desktop / CLI > Console Autologin.
    - **Advanced Options**: Disable 'Overscan'.

3. Reboot.
4. Update and upgrade: ```sudo apt-get update -y && apt-get upgrade```

----
### Minimum Environment 

1. X Server ([X.Org](https://www.x.org/wiki/))
2. Window manager ([Openbox](http://openbox.org))

```sh
sudo apt-get install --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox
```

3. Web Browser (Chromium)

```sh
sudo apt-get install --no-install-recommends chromium-browser
```
----
### Rotate Touch Screen Display

1. Edit ```/boot/config.txt```
```sh
lcd_rotate=2
```
----
### Openbox Configuration:

1. Edit ```/etc/xdg/openbox/autostart```

```sh
# Disable any form of screen saver / screen blanking / power management

xset s off
xset s noblank
xset -dpms

# Allow quitting the X server with CTRL - ALT - Backspace

setxkbmap -option terminate:ctrl_alt_bksp

# Start Chromium in kiosk mode.
# Avoid session restore bubbles with --app=<your-url> after crashed/forced shutdown.
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":[^"]\+"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
chromium-browser --remote-debugging-port=9222 --disable-infobars --kiosk --app=http:/localhost:3000/

```
-----

### Start X automatically on boot

1. Edit ```.bashrc``` and add:
  ```sh
  [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor 
  ```
2. Reboot
