const mainView = require('./render-routes/main-view');
const manualView = require('./render-routes/manual-view');
const successView = require('./render-routes/success-view');
const reservationError = require('./render-routes/reservation-error-view');
const openingError = require('./render-routes/opening-error-view');
const setLanguage = require('./render-routes/language');
const warningView = require('./render-routes/warning-view');
const handleOpenGateRouter = require('./providers-routes/handleOpenGate');
const statusCheckRouter = require('./providers-routes/statusCheck');

module.exports = {
    mainView,
    manualView,
    openingError,
    reservationError,
    successView,
    setLanguage,
    warningView,
    handleOpenGateRouter,
    statusCheckRouter
};