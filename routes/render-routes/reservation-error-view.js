const express = require('express');
const reservationError = express.Router();
const errorStore = require('./../../providers/globals/errorTextStore');
const { errorTextStore: errorText, devErrorStore: errorDev } = errorStore;

reservationError.get('/', function(req, res, next) {

  res.render('reservation-error-view', { 
    title: 'ERROR',
    reason: errorText.get(),
    error_dev_message: errorDev.get()
    }); 

    console.log('');
    console.log('\x1b[1m%s\x1b[0m', '************************************************');
    console.log("RESERVATION ERROR");
  });

module.exports = reservationError;