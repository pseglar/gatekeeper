const express = require('express');
const openingError = express.Router();
const errorText = require('../../providers/globals/errorTextStore').errorTextStore;
const i18n = require('i18n');

openingError.get('/', function (req, res, next) {

  errorText.set(['Lo sentimos','Somos unos pardillos']);
  res.clearCookie('language');

  res.render('opening-error-view', {
    title: i18n.__("opening-error-view.title-text"),
    reason: errorText.get()
  });

  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("OPENING ERROR");
});

module.exports = openingError;