const express = require('express');
const i18n = require('i18n');
const mainView = express.Router();
const errorStore = require('../../providers/globals/errorTextStore');
const scannerStatus = require('../../providers/globals/scannerStatus');
const handleLanguage = require('./../../providers/handle-language/index');
const { errorTextStore: errorText, devErrorStore: errorDev } = errorStore;
const warningStore = require('../../providers/globals/warningStore');
const { warningImages, warningMessages, warningStatus } = warningStore;

mainView.get('/', function (req, res, next) {

  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("PÁGINA PRINCIPAL");

  handleLanguage(req.cookies.language);

  scannerStatus.set(true);
  console.log("\x1b[7m%s\x1b[0m", " Scanner Resumed ");

  resetStorages(undefined);

  res.render('main-view', {
    main_text: i18n.__("main.welcome"),
    scanner_instructions: i18n.__("main.scanner-instructions"),
    manual_btn_text: i18n.__("main.manual-btn-text"),
  });

});

const resetStorages = (value) => {
  errorText.set(value);
  errorDev.set(value);
  warningImages.set(value);
  warningMessages.set(value);
  warningStatus.set(value);
};

module.exports = mainView;