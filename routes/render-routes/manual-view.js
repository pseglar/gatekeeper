const express = require('express');
const manualView = express.Router();
const i18n = require('i18n');

manualView.get('/', function (req, res, next) {

  res.render('manual-view', {
    text_info: i18n.__("manual-view.info-text"),
    placeholder: i18n.__("manual-view.placeholder"),
    send_btn_text: i18n.__("manual-view.send-btn-text"),
    back_btn_text: i18n.__("common.back-btn-text"),
    waiting_text: i18n.__("common.sending-req-text"),
    code_error: i18n.__('manual-view.code-error')
  });

  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("MANUAL VIEW");
});

module.exports = manualView;