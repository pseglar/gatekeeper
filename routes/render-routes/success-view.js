const express = require('express');
const instructionsView = express.Router();
const i18n = require('i18n');
const instructionsImgs = require('./../../providers/globals/instructionsStore');

instructionsView.get('/', function (req, res, next) {

  //cambiar por res.LANG o lo q sea y cambiar el nombre de archivo

  res.clearCookie('language');
  
  res.render('success-view', {  
    opening_error_text_1: i18n.__("instructions.opening-error-text-1"),
    opening_error_text_2: i18n.__("instructions.opening-error-text-2"),
    opening_error_text_3: i18n.__("instructions.opening-error-text-3"),
    report_btn_text: i18n.__("instructions.report-btn-text"),
    instructions: instructionsImgs.get()
  });

  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("SUCCESS PAGE");
});

module.exports = instructionsView;