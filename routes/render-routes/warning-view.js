const express = require('express');
const warningView = express.Router();
const i18n = require('i18n');
const warningStore = require('../../providers/globals/warningStore');
const { warningImages, warningMessages } = warningStore;


warningView.get('/', function (req, res, next) {

  res.render('warning-view', {
    title: i18n.__("warning-view.title-text"),
    warning_images: warningImages.get(),
    warning_messages: warningMessages.get(),
    warning_btn_text: i18n.__("warning-view.warning-btn-text")
  });

  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("WARNING");
});

module.exports = warningView;