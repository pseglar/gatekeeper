const express = require('express');
const setLanguage = express.Router({ mergeParams: true }); //allow to access paramenters in parent routes
const handleLanguage = require('./../../providers/handle-language');

setLanguage.get('/', function(req, res, next) {
  if(req.cookies.language) res.clearCookie('language');
  res.cookie('language', req.params.language);
  handleLanguage(req.params.language); 
  res.redirect('back');
  console.log('');
  console.log('\x1b[1m%s\x1b[0m', '************************************************');
  console.log("CAMBIO IDIOMA");
});

module.exports = setLanguage;