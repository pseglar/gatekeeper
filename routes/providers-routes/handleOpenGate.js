const express = require('express');
const handleOpenGateRouter = express.Router();

const openGate = require('../../providers/gate');

handleOpenGateRouter.post('/', openGate);

module.exports = handleOpenGateRouter;