const express = require('express');
const statusCheckRouter = express.Router();

const statusCheck = require('../../providers/status-check');

statusCheckRouter.get('/', statusCheck);

module.exports = statusCheckRouter;