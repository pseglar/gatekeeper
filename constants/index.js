fs = require('fs'); 

const CONSTANTS = () => {
  return JSON.parse(fs.readFileSync('constants.json', 'utf8'));
};

module.exports = CONSTANTS();