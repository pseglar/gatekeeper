# Touch Screen Display (Raspberry Pi 3 B+)

----

>With the LCD laying flat and the brown ribbon cable assembly facing you, locate the DSI ribbon connector on the left. 
Remove the connector tab gently and insert the DSI. The opposite's DSI end should be blue.

1. Screw to the rasp.
2. Connect the cables:
    - To the Display:
      - Red on the 5V pin (far right).
      - Black on the GND Pin (far left).
      - Yellow on the SCL pin (next to the black).
      - Green on thr SDA pin (between yellow and red).
    - To the Rasp: 
      - Red to Pin 2 (If it is externally powered, reserve pin for a relay.)
      - Greeen to Pin 3
      - Yellow to Pin 5
      - Black to pin 6


-----
###  Install packages:
```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get install raspberrypi-ui-mods
sudo apt-get install raspberrypi-net-mods
```
-----



### Rotate Touch Screen Display

1. Edit ```/boot/config.txt```
```sh
lcd_rotate=2
```