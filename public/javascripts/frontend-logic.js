// Socket io connection.

const socket = io({
  transports: ['websocket'],
  reconnection: true,
  reconnectionAttempts: 3,
  timeout: 30000, // connection timeout before a connect_error and connect_timeout events are emitted
  reconnectionDelay: 500,
  upgrade: false,
  rejectUnauthorized: false,
  agent: false
});

socket.on('connect', (data) => console.log("Socket conectado"));

socket.on('disconnect', (reason) => {
  console.log(`Socket desconectado: ${socket.disconnected}`);
  console.log(`Razón : ${reason}`);
});

// Socket emission and touchscreen keyboard

window.addEventListener('load', () => {

  const codeInput = document.getElementById('manual-input');
  const sendBtn = document.getElementById('manual-btn');
  const warningBtn = document.getElementById('warning-view-btn');
  const reportBtn = document.getElementById('report-btn');
  const keys = document.getElementsByClassName('key');
  const closeKeyboard = document.getElementsByClassName('close-keyboard');

  // Socket emissions

  if (sendBtn) {
    sendBtn.addEventListener('click', () => {

      onWaitingView();

      try {

        if (checkCodeRegex(codeInput.value)) socket.emit('manual-btn', codeInput.value);

      } catch (err) {

        setDisplay('waiting', 'none');
        setDisplay('code-error', 'flex');
        setTimeout(function () {
          setDisplay('code-error', 'none');
          setDisplay('content', 'flex');
          codeInput.value = '';
        }, 4000);
      }
    });
  }

  if (warningBtn) {
    warningBtn.addEventListener('click', () => {

      onWaitingView();

      try {

        socket.emit('warning-btn');

      } catch (err) {

        setDisplay('waiting', 'none');
        setDisplay('code-error', 'flex');
        setTimeout(function () {
          setDisplay('code-error', 'none');
          setDisplay('content', 'flex');
        }, 4000);
      }
    });
  }

  const checkCodeRegex = (input) => {

    const regEx = /^[0-9]{9,10}$/;

    if (!!input.match(regEx)) {
      return true;
    } else {
      throw 'InvalidReFe';
    }
  };

  if (reportBtn) {
    reportBtn.addEventListener('click', () => socket.emit('report-btn', { id: event.target }));
  }

  // Touchscreen keyboard 

  if (codeInput) {
    codeInput.addEventListener('click', () => {
      setDisplay('return', 'none');
      setDisplay('keyboard', 'flex');
    });
  }

  if (keys) {
    for (let i = 0; i < keys.length; i++) {
      keys[i].addEventListener("click", () => {
        if (i === (keys.length - 1)) {
          codeInput.value = codeInput.value.slice(0, -1);
        } else {
          let keyValue = keys[i].innerHTML;
          codeInput.value += keyValue;
        }
      });
    }
  }

  if (closeKeyboard[0]) {
    closeKeyboard[0].addEventListener('click', function () {
      setDisplay('return', 'flex');
      setDisplay('keyboard', 'none');
    });
  }
});

// Socket event listeners

socket.on('redirect', ({ path }) => redirectTo(path));

socket.on('waiting', () => onWaitingView());

// Redirect function.

const redirectTo = (path) => {
  onWaitingView();
  window.location.href = path;
};

// Waiting class - CSS related.

const onWaitingView = () => {
  setDisplay('content', 'none');
  setDisplay('waiting', 'flex');
};

// Set display CSS rule.

const setDisplay = (idName, displayMode) => document.getElementById(idName).style.display = displayMode;

// Redirect paths.

const goToMainPage = () => {
  setDisplay('code-error', 'none');
  setDisplay('dev-error', 'none');
  setDisplay('content', 'flex');
  redirectTo('/');
};

const goToManualInput = () => redirectTo('/manual-opening');

const goToReservationError = () => redirectTo('/reservation-error');

const goToReservationSuccess = () => redirectTo('/success');

const goToOpeningError = () => redirectTo('/opening-error');

const changeLanguage = (language) => redirectTo(`/setLocale/${language}`);


// Developer error sneak.

const showDevError = (errorDev) => {

  if (errorDev) {
    setDisplay('dev-error', 'flex');
    setDisplay('content', 'none');
  } else {
    return;
  }

};

// Socket events. 

socket.on('reconnecting', (attemptNumber = 5) => console.log('Reconectando'));

socket.on('connect_error', (error) => console.log('Error de conexión socket cliente'));

socket.on('reconnect_error', (error) => console.log('Error de reconexión'));

socket.on('error', (error) => {
  console.log('Error en socket io');
  console.log(error);
});

socket.on('ping', () => console.log('*** ping ***'));

socket.on('pong', () => console.log('+++ pong +++'));