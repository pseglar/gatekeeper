fs = require('fs');

const CONFIG = () => {
  return JSON.parse(fs.readFileSync('config.json', 'utf8'));
};
const { BASE_URL, GATE_ID, vendorId, productId, GATE_PIN, ALERT_SUCCESS_PIN, ALERT_ERROR_PIN, ALERT_DURATION, PIN_TIME_OUT } = CONFIG();

console.log(`BASE_URL: ${ BASE_URL }`);
console.log(`GATE_ID: ${ GATE_ID }`);
console.log(`vendorId: ${ vendorId }`);
console.log(`productId: ${ productId }`);
console.log(`GATE_PIN: ${ GATE_PIN }`);
console.log(`ALERT_SUCCESS_PIN: ${ ALERT_SUCCESS_PIN }`);
console.log(`ALERT_ERROR_PIN: ${ ALERT_ERROR_PIN }`);
console.log(`ALERT_DURATION: ${ ALERT_DURATION }`);
console.log(`PIN_TIME_OUT: ${ PIN_TIME_OUT }`);

module.exports = CONFIG();