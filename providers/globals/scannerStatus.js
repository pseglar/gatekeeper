let _bool = true;

const ScannerStatus = {
  set: status => _bool = status,
  get: () => _bool
};

Object.freeze(ScannerStatus);

module.exports = ScannerStatus;