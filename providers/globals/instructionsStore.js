let _data = [];

const InstructionsStore = {
  set: instructions => _data = instructions,
  get: () => _data
};

Object.freeze(InstructionsStore);

module.exports = InstructionsStore;