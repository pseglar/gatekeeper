let _data = [];
let _data2 = [];
let _data3 = false;

module.exports = { 
  warningImages: {
    set: images => _data = images,
    get: () => _data
  },

  warningMessages: {
    set: messages => _data2 = messages,
    get: () => _data2
  },

  warningStatus: {
    set: status => _data3 = status,
    get: () => _data3
  }
};

Object.freeze(module.exports.warningImages);
Object.freeze(module.exports.warningMessages);
Object.freeze(module.exports.warningStatus);