let _data = [];
let _data2 = [];

module.exports = { 
  errorTextStore: {
    set: reasons => _data = reasons,
    get: () => _data
  },
  
  devErrorStore: {
    set: message => _data2 = message,
    get: () => _data2
  }
};

Object.freeze(module.exports.errorTextStore);
Object.freeze(module.exports.devErrorStore);