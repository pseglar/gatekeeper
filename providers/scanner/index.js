/* Get devices*/
const getDevices = require('usb-barcode-scanner').getDevices;
const connectedHidDevices = getDevices();

const UsbScanner = require('usb-barcode-scanner').UsbScanner;
const handleCodeResponse = require('./../handle-response');
const handleError = require('../handle-error');
const codeCheck = require('./../code-check/index');
const CONFIG = require('../../config');
const CONSTANTS = require('../../constants/index');
const socketApi = require('./../socket/socket');
const scannerStatus = require('./../globals/scannerStatus');

const { vendorId, productId, GATE_ID: id_gate } = CONFIG;
const { ERROR_STATUS } = CONSTANTS;
const scannerApi = {};

scannerApi.scanner = new UsbScanner({ vendorId, productId });

scannerApi.startListening = () => {

  scannerApi.scanner.on('data', async (code) => {

    console.log("\x1b[1m%s\x1b[0m", '>>> Scanned code: ' + code);

    if (scannerStatus.get()) socketApi.setWaitingStatus();

    try {

      const response = await codeCheck(id_gate, code);
      handleCodeResponse(response.data);

    } catch (error) {

      const errorMsg = handleError(error);

      if (errorMsg === ERROR_STATUS.SCANNER_OFF) {

        return;

      } else {

        if (error && error.response.data.reason) {
          socketApi.reservationErrorRedirect(error.response.data, null);
        } else {
          socketApi.reservationErrorRedirect(null, errorMsg);
        }
      }
    }
  });
};

module.exports = scannerApi;