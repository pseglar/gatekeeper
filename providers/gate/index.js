const onOffGatePin = require('../gpio');
const handleError = require('../handle-error');

const openGate = (relayNumber, openTime) => {
    try {
        const response = onOffGatePin(relayNumber, openTime);
        return response;
    } catch(error) {
        handleError(error);
    }
};

module.exports = openGate;