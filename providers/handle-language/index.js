const i18n = require('i18n');

const handleLanguage = (language) => {  
  if(language) i18n.setLocale(language); else i18n.setLocale('es'); //i18n.defaultLocale
};

module.exports = handleLanguage;