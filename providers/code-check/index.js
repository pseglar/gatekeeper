const axios = require('axios');
const scannerStatus = require('./../globals/scannerStatus');
const CONSTANTS = require('./../../constants/index');
const { ERROR_STATUS } = CONSTANTS;


const codeCheck = async (id_gate, code) => {

	const url = `/barcode/${code}`;

	//HACER EL BEARER TOKEN AUTH CON ID_GATE

	if (scannerStatus.get()) {

		try {
			const response = await axios.get(url, { /*headers */ });
			scannerStatus.set(false);
			console.log("\x1b[7m%s\x1b[0m", " Scanner Stopped ");
			return response;
		} catch (err) {
			throw err;
		}

	} else {
		throw new Error(ERROR_STATUS.SCANNER_OFF);
	}
};

module.exports = codeCheck;