const CONSTANTS = require('./../../constants/index');
const { ERROR_STATUS } = CONSTANTS;
const devErrorStore = require('../globals/errorTextStore').devErrorStore;

module.exports = handleError = (error) => {

  let { message: errorMsg } = error;

  devErrorStore.set(errorMsg);

  switch(errorMsg) {
    case 'timeout of 30000ms exceeded':
      errorMsg = 'Error de conexión';
      console.log('Request TIMEOUT');
      break;
    case ERROR_STATUS.SCANNER_OFF:
      console.log("\x1b[1m%s\x1b[0m", 'scanner ignorado, codeCheck no procesado');
      break;
  }
  
  return errorMsg;
};