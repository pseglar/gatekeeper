const socket_io = require('socket.io');
const terminalLog = require('./terminalLog');
const CONFIG = require('./../../config/index');
const { GATE_ID: id_gate } = CONFIG;

const io = socket_io({
  'transports': ['websocket'],
  'pingTimeout': 5000,
  'pingInterval': 2000,
  'allowUpgrades': false
});

// Providers.

const codeCheck = require('../code-check/index');
const handleCodeResponse = require('./../handle-response/index');
const handleError = require('../handle-error');

// Store providers.

const errorText = require('./../globals/errorTextStore').errorTextStore;
const instructionsImgs = require('./../globals/instructionsStore');
const warningStore = require('./../globals/warningStore');
const { warningImages, warningMessages, warningStatus } = warningStore;

// SocketApi.

const socketApi = {};
socketApi.io = io;

// Stored response reservation needed if the response reservation have warning properties.

let _reservation;

socketApi.io.on('connection', (client) => {

  terminalLog.clientConnected(client);

  client.on('disconnect', (reason) => {

    terminalLog.clientDisconnected(client);
    console.log(`Reason: ${reason}`);

    if (reason === 'ping timeout') {
      console.log(`${client.id} >> ping timeout`);
    } else if (reason === 'server namespace disconnect') {
      console.log(`${client.id} >> server namespace disconnect`);
    }

  });

  client.on('manual-btn', async (code) => {

    try {

      const response = await codeCheck(id_gate, code);
      handleCodeResponse(response.data);

    } catch (error) {

      const errorMsg = handleError(error);

      if (error && error.response.data.reason) {
        socketApi.reservationErrorRedirect(error.response.data, null);
      } else {
        socketApi.reservationErrorRedirect(null, errorMsg);
      }
    }
  });

  client.on('warning-btn', () => {
    try {
      
      //stored response
      handleCodeResponse(_reservation);
      _reservation = null;

    } catch (error) {

      const errorMsg = handleError(error);

      if (error && error.response.data.reason) {
        socketApi.reservationErrorRedirect(error.response.data, null);
      } else {
        socketApi.reservationErrorRedirect(null, errorMsg);
      }
    }
  });
});

socketApi.successRedirect = (reservation) => {
  let { actions: [{ final_images: finalImages }] } = reservation;
  destination = '/success';
  instructionsImgs.set(finalImages);
  socketApi.io.sockets.emit('redirect', { path: destination });
};

socketApi.warningRedirect = (reservation) => {

  let { actions: [{ warning_image: warningImage,
    warning_message: warningMessage }] } = reservation;

  if (typeof warningImage === 'string') warningImage = warningImage.split();

  destination = '/warning';
  warningImages.set(warningImage);
  warningMessages.set(warningMessage);
  warningStatus.set(true);
  _reservation = reservation;
  socketApi.io.sockets.emit('redirect', { path: destination });
};

socketApi.reservationErrorRedirect = (response, errorMsg) => {

  destination = '/reservation-error';

  if (response && response.reason) {

    if (typeof response.reason === 'string') response.reason = response.reason.split();
    errorText.set(response.reason);

  } else {
    errorText.set(['ERROR TÉCNICO', [`${errorMsg || ''}`]]);
  }

  socketApi.io.sockets.emit('redirect', { path: destination });
};

socketApi.redirect = (destination) => {
  socketApi.io.sockets.emit('redirect', { path: destination });
};

socketApi.setWaitingStatus = () => {
  socketApi.io.sockets.emit('waiting', true);
};

module.exports = socketApi;