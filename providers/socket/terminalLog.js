const terminalLog = {

  clientConnected: (client) => console.log('\x1B[30;1;42m%s\x1b[0m', 'CLIENT >> Socket connected: ', client.connected, ` ||| New socket id: ${client.id}`),

  clientEmit: (data) => console.log('\x1b[44m%s\x1b[0m', "CLIENT EMIT >> ", data),

  clientDisconnected: (client) => console.log('\x1b[41m%s\x1b[0m', `CLIENT >> Socket ${client.id} disconnected`)
};

module.exports = terminalLog;