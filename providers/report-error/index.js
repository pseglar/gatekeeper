const axios = require('axios');
const i18n = require('i18n');
const handleError = require('./../handle-error');

const reportError = async (code) => {

    try {
        const url = `/barcode/error`;
        const response = await axios.post(url, { lang: i18n.getLocale() });
        return response.data;
    } catch (err) {
        handleError(err);
    }
};

module.exports = reportError;