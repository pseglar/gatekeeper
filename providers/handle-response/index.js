const openGate = require('../gate');
const CONSTANTS = require('./../../constants/index');
const { RESPONSE_STATUS } = CONSTANTS;
const warningStatus = require('../globals/warningStore').warningStatus;
const handleLanguage = require('./../handle-language/index');

/* Solving a circular dependency with a nasty trick, I'm sorry for existing */
// Do not change this order unless you know how to solve the dependency.
module.exports = handleCodeResponse = (reservation) => manageResponse(reservation);
const socketApi = require('./../socket/socket');

const manageResponse = async (reservation) => {

  const { status, actions, lang } = reservation;
  const [{ warning_message: warningMessage }] = actions;

  if (lang) handleLanguage(lang);

  switch (status) {

    case RESPONSE_STATUS.OK:

      if (reservation && warningMessage && !warningStatus.get()) {

        return socketApi.warningRedirect(reservation);

      } else {

        for (let i = 0; i < actions.length; i++) {

          const delay = actions[i].delay;
          const openTime = actions[i].open_time;
          const relayNumber = actions[i].open;

          try {
            await setTimeout(() => openGate(relayNumber, openTime), delay * 1000);
          } catch (err) {
            throw err;
          }
        }

        socketApi.successRedirect(reservation);
      }
      
      break;

    default:
      socketApi.reservationErrorRedirect(reservation, null);
      break;
  }
};