const Gpio = require('onoff').Gpio;
const CONFIG = require('../../config');
const { GATE_PIN, GATE_PIN_2 } = CONFIG;

const rly = new Gpio(GATE_PIN, 'in');
const rly2 = new Gpio(GATE_PIN_2, 'in'); 
let relay;

const onOffGatePin = (relayNumber = 1, openTime) => {
  assignRelay(relayNumber);
  gateOn();
  setTimeout(() => gateOff(), openTime * 1000);
  return console.log("ABRIENDO PUERTAS");
};

const gateOn = () => {
  relay.setDirection("out");
  relay.writeSync(1);
};

const gateOff = () => {
  relay.writeSync(0);
  relay.setDirection("in");
};

const assignRelay = (relayNumber) => relay = relayNumber == 1 ? rly : rly2;

module.exports = onOffGatePin;